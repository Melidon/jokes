import 'package:flutter/material.dart';
import 'package:jokes/models/joke.dart';
import 'package:jokes/models/joke_category.dart';
import 'package:jokes/services/joke_service.dart';

class JokePage extends StatefulWidget {
  const JokePage(this._jokeCategory, {Key? key}) : super(key: key);

  final JokeCategory _jokeCategory;

  @override
  State<JokePage> createState() => _JokePageState();
}

class _JokePageState extends State<JokePage> {
  final _jokeService = JokeService();
  late Future<Joke?> _joke;

  @override
  void initState() {
    super.initState();
    getJoke();
  }

  void getJoke() {
    _joke = _jokeService.getJoke(widget._jokeCategory);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._jokeCategory.name),
      ),
      body: FutureBuilder(
        future: _joke,
        builder: (BuildContext context, AsyncSnapshot<Joke?> snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.data == null) {
            return const Text('Error loading joke');
          }
          return Padding(
            padding: const EdgeInsets.all(32.0),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  JokeWidget(snapshot.data!),
                  ElevatedButton(
                    onPressed: () {
                      getJoke();
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Next',
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            ?.copyWith(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class JokeWidget extends StatelessWidget {
  const JokeWidget(this._joke, {Key? key}) : super(key: key);

  final Joke _joke;

  @override
  Widget build(BuildContext context) {
    switch (_joke.type) {
      case 'single':
        {
          return SelectableText(
            _joke.joke!,
            style: Theme.of(context)
                .textTheme
                .headline5
                ?.copyWith(color: Colors.black),
          );
        }
      case 'twopart':
        {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SelectableText(
                _joke.setup!,
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    ?.copyWith(color: Colors.black),
              ),
              const SizedBox(height: 32),
              SelectableText(
                _joke.delivery!,
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    ?.copyWith(color: Colors.black),
              ),
            ],
          );
        }
      default:
        {
          return Container();
        }
    }
  }
}
