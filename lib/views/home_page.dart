import 'package:flutter/material.dart';
import 'package:jokes/models/joke_category.dart';
import 'package:jokes/views/joke_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Jokes'),
      ),
      body: ListView.separated(
        itemBuilder: _listItemBuilder,
        separatorBuilder: (BuildContext context, int index) {
          return const Divider();
        },
        itemCount: JokeCategory.values.length,
      ),
    );
  }

  Widget _listItemBuilder(BuildContext context, int index) {
    return TextButton(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Text(
          JokeCategory.values[index].name,
          style: Theme.of(context)
              .textTheme
              .headline4
              ?.copyWith(color: Colors.lightBlue),
        ),
      ),
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => JokePage(JokeCategory.values[index]),
          ),
        );
      },
    );
  }
}
