import 'package:jokes/models/joke.dart';
import 'package:jokes/models/joke_category.dart';
import 'package:http/http.dart' as http;

class JokeService {
  static const _baseUrl = 'https://v2.jokeapi.dev/joke';

  final _httpClient = http.Client();

  Future<Joke?> getJoke(JokeCategory jokeCategory) async {
    final url = Uri.parse(_baseUrl + '/' + jokeCategory.name);
    final response = await _httpClient.get(url);
    if (response.statusCode != 200) {
      return null;
    }
    final json = response.body;
    return jokeFromJson(json);
  }
}
