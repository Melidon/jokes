enum JokeCategory {
  Any,
  Programming,
  Miscellaneous,
  Dark,
  Pun,
  Spooky,
  Christmas
}
