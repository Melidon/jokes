# Jokes

This is my homework for the Flutter-based Software Development subject.

You can try the application [here](https://flutter-jokes.web.app/).

The source code is available on [GitLab](https://gitlab.com/Melidon/jokes).
